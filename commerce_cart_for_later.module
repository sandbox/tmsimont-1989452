<?php

/**
 * Implements hook_init()
 */
function commerce_cart_for_later_init() {
  //when a user visits the cart page, restore any previously saved carts and refresh
  if (arg(0)=='cart') {
    commerce_cart_for_later_check_for_saved_carts();
  }
}

/**
 * Implements hook_form_alter()
 */
function commerce_cart_for_later_form_alter(&$form, &$form_state, $form_id) {
  /**
   * On add to cart form, check for previously saved carts and refresh
   */
  if (strpos($form_id,"commerce_cart_add_to_cart_form")===0) {
    commerce_cart_for_later_check_for_saved_carts();
  }
}

/**
 * Implements hook_commerce_order_status_info().
 */
function commerce_cart_for_later_commerce_order_status_info() {
  $order_statuses = array();
  //define a new cart status called "Saved for later"
  $order_statuses['saved_cart'] = array(
    'name' => 'saved_cart',
    'title' => t('Saved for later'),
    'weight' => 5,
    'state' => 'cart',
    'cart' => TRUE,
  );

  return $order_statuses;
}

/**
 * This returns a new order for a given user while saving
 * any existing cart order for later
 * @param $user
 *  the user to tie the order to
 * @param $show_message_on_restore
 *  should text be displayed when the saved cart is restored
 */
function _commerce_cart_for_later_get_new_order($user, $show_message_on_restore = true) {
  // see if there is an existing cart order for the user
  $order = commerce_cart_order_load($user->uid);
  
  // if so, save it for later
  if ($order && _commerce_cart_for_later_user_order_to_save($order)) {
    $order->data['show_message_on_restore'] = $show_message_on_restore;
    commerce_order_status_update($order, 'saved_cart');
    // wipe out any previously "saved" orders to prevent infinite "saved order" additions
    $all_cart_orders = _commerce_cart_for_later_commerce_get_all_cart_orders($user->uid);  
    foreach($all_cart_orders as $existing) {
      if ($existing->order_number != $order->order_number) {
        commerce_order_delete($existing->order_id);
      }
    }
    $old_saved = true;
  } else {
    $old_saved = false;
  }
  
  //build a new empty order
  if (empty($order) || !$order || $old_saved) {
    $order = commerce_cart_order_new($user->uid);
  }
  
  return $order;
}

/**
 * Determine if an order has been started for the user, and if that order has
 * any items
 */
function _commerce_cart_for_later_user_order_to_save($order) {
  $quantity = 0;
  if($order) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      $quantity += $line_item_wrapper->quantity->value();
    }
  } else {
    return false;
  }
  if ($quantity==0) {
    return false;
  }
  return true;
}

/**
 * Retrieve all cart status orders related to a given user
 * @param $uid int
 *  the user id of the user
 */
function _commerce_cart_for_later_commerce_get_all_cart_orders($uid) {
  $orders = array();
  
  // Create an array of valid shopping cart order statuses.
  $status_ids = array_keys(commerce_order_statuses(array('cart' => TRUE)));
  
  // If a customer uid was specified...
  if ($uid) {
    // get a list of all of the shopping carts associated with this user
    $result = db_query('SELECT order_id FROM {commerce_order} WHERE uid = :uid AND status IN (:status_ids) ORDER BY order_id DESC', array(':uid' => $uid, ':status_ids' => $status_ids))->fetchAll();
  }
  else {
    // Otherwise look for a shopping cart order ID in the session.
    if (commerce_cart_order_session_exists()) {
      // We can't trust a user's session to contain only their order IDs, as
      // they could've edited their session to try and gain access to someone
      // else's order data. Therefore, this query restricts its search to
      // orders assigned to anonymous users using the same IP address to try
      // and mitigate risk as much as possible.
      $result = db_query('SELECT order_id FROM {commerce_order} WHERE order_id IN (:order_ids) AND uid = 0 AND hostname = :hostname AND status IN (:status_ids) ORDER BY order_id DESC', array(':order_ids' => commerce_cart_order_session_order_ids(), ':hostname' => ip_address(), ':status_ids' => $status_ids))->fetchAll();
    }
  }
  
  if (isset($result)) {
    foreach($result as $record) {
      $orders[] = commerce_order_load($record->order_id);
    }
  }
  
  return $orders;
}

/**
 * restore a "saved for later" cart order back to normal cart status
 */
function _commerce_cart_for_later_restore_saved_cart($order) {
  if ($order->status=='saved_cart') {
    if (isset($order->data['show_message_on_restore']) && $order->data['show_message_on_restore'] === TRUE) {
      drupal_set_message(t("The shopping cart items you had previously selected to \"Save for later\" have been restored to your cart."));
    }
    commerce_order_status_update($order, 'cart');
  }
}

/**
 * check to see if the current user has any saved carts
 * @param $reload (boolean)
 *  optional.  defaults to true.  if true, the current page is reloaded when 
 *  this function is called, which refreshes the page with the user's cart restored.
 */
function commerce_cart_for_later_check_for_saved_carts($reload = true) {
  global $user;
  $cart_order = @commerce_cart_order_load($user->uid);
  $all_cart_orders = _commerce_cart_for_later_commerce_get_all_cart_orders($user->uid);
  
  //assume there's no reason to not restore a cart order when this function is run if the current order is empty
  if ($cart_order) {
    $auto_restore = count($cart_order->commerce_line_items)>0?false:true;
  }
    
  foreach($all_cart_orders as $order) {
    if ($order->order_number != $cart_order->order_number) {
    
      //determine size of order
      $quantity = 0;
      $wrapper = entity_metadata_wrapper('commerce_order', $order);
      foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
        $quantity += $line_item_wrapper->quantity->value();
      }
      
      if ($quantity > 0 && $auto_restore && $reload) {
        commerce_order_delete($cart_order->order_id);
        _commerce_cart_for_later_restore_saved_cart($order);
        
        //TODO: cleaner referesh of the current page?
        $path = $_SERVER['REQUEST_URI'];
        if ($path[0] == "/") {
          $pathparts = explode("/",$path);
          unset($pathparts[0]);
          $path = implode("/",$pathparts);
        }
        drupal_goto($path);
        
        return;
      }
      
    }
    
  }
  
}