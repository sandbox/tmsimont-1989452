<?php

/**
 * @file
 * Rules integration for Commerce Save Cart for Later.
 */

/**
 * Implements hook_rules_action_info().
 *
 * Provides an action to restore a user's "saved cart" order
 */
function commerce_cart_for_later_rules_action_info() {
  $actions = array();
  $actions['commerce_cart_for_later_restore_saved_carts'] = array(
    'label' => t('Restore a user\'s saved cart order.'),
    'group' => t('Commerce Save Cart for Later'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order in checkout'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_cart_for_later_rules_restore_saved_carts',
    ),
  );
  return $actions;
}

/**
 * Action callback: completes checkout for the given order.
 */
function commerce_cart_for_later_rules_restore_saved_carts($order) {
  //$order is the just-completed order, check for a cart order with the same UID
  $all_cart_orders = _commerce_cart_for_later_commerce_get_all_cart_orders($order->uid);
  foreach($all_cart_orders as $cart_order) {
    if ($cart_order->order_number != $order->order_number) {
      _commerce_cart_for_later_restore_saved_cart($cart_order);
    }
  } 
}