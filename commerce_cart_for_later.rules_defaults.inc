<?php

/**
 * @file
 * Default rule configurations for Commerce Save Cart for Later.
 */
function commerce_cart_for_later_default_rules_configuration() {
  $rules_restore_saved_cart = '{ "rules_restore_saved_cart" : {
    "LABEL" : "Restore saved cart",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "10",
    "REQUIRES" : [ "commerce_cart_for_later", "commerce_checkout" ],
    "ON" : [ "commerce_checkout_complete" ],
    "DO" : [
      { "commerce_cart_for_later_restore_saved_carts" : { "commerce_order" : [ "commerce_order" ] } }
    ]
  }
}';
  $configs['rules_restore_saved_cart'] = rules_import($rules_restore_saved_cart);

  return $configs;
}
